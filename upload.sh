#!/bin/sh
##------------------------------------------------------------------------------------------------##
## Bitcasa に upload するようのスクリプトです。中身は単純に指定されたファイルもしくはディレクトリ ##
## を、同じく指定されたパスへrsyncするものであるが、ディレクトリをuploadする場合はBitcasa特有のキ ##
## ャッシュの問題を考慮してある。その問題とはBitcasaの仕様で、Bitcasa側で予め指定された容量分のキ ##
## ャッシュにエンコードされたファイルが一時的に保存され、その後一定のスピードで少しづつuploadされ ##
## ていくという仕様が、キャッシュがすべて使われてしまうと、rsyncがそれ以上コピー不可になってしまい## 
## ERRORを返してしまうというものである。そのため、連続してuploadする場合、1つ前のファイルがキャッ ##
## シュから完全に無くならないと次のuploadが始まらないようにしてある。                             ##
##------------------------------------------------------------------------------------------------##

usage_exit() {
  echo "Usage: $0 [-f] SRC_FILE DEST_FILE"
  echo "\t or \t [-d] SRC_DIR DEST_DIR"
  exit 1
}

if [ $# -ne 3 ]; then
  usage_exit
fi


while getopts :f:d:h opt
do
  case $opt in
  f)
    FLG_FILE="TRUE";
    SRC="$OPTARG";;
  d)
    FLG_DIR="TRUE";
    SRC="$OPTARG" ;;
  h)
    usage_exit ;;
  \?)
    usage_exit ;;
  esac
done

SCRIPT_DIR=$(cd $(dirname $0); pwd)
DEST="$3"


# File upload
if [ "$FLG_FILE" == "TRUE" ];then
  CHK_EXIST=`${SCRIPT_DIR}/chk_exist.sh "$SRC" "$DEST"`
  if [ $CHK_EXIST == "nothing" ]; then
    rsync -a "$SRC" "$DEST"
  fi
fi

# Directory upload
if [ "$FLG_DIR" == "TRUE" ];then 

  # Check dir exsist
  DIR_NAME=`basename "$SRC"` 
  CHK_EXIST=`${SCRIPT_DIR}/chk_exist.sh "$DIR_NAME" "$DEST"`
  if [ "$CHK_EXIST" == "nothing" ]; then
    mkdir "${DEST}/${DIR_NAME}"
  fi

  for FILE in $SRC/*
  do
    CHK_EXIST=`${SCRIPT_DIR}/chk_exist.sh "$FILE" "${DEST}/${DIR_NAME}"`
    if [ $CHK_EXIST == "nothing" ]; then
      rsync -a "$FILE" "${DEST}/${DIR_NAME}"
    fi

    while :
    do
      sleep 10
      leftovers=`du -s ~/Library/Caches/com.bitcasa.Bitcasa/Data/bks/outgoing|awk '{print $1}'`
      if [ "$leftovers" == "0" ]; then
        break
      fi
    done 
  done
fi 
exit 0
