#!/bin/sh
#-----------------------------------------------------------------------------#
# ファイルの存在確認用のスクリプト                                            #
# ただし、単純にあるかどうかだけを判断しているだけで内容の違いは識別できない。#
#-----------------------------------------------------------------------------#
usage_exit() {
  echo "Usage: $0 [file | dir] [dest pass]"
  exit 1
}

if [ $# -ne 2 ]; then
  usage_exit
fi

FILE_NAME=`basename "$1"`
FILE_PATH=$2

if [ -e "${FILE_PATH}/${FILE_NAME}" ]; then
  echo "existed"
else
  echo "nothing"
fi

exit 0

