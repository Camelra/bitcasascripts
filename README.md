# Bitcasa Scripts

## upload.sh
### 機能
* ファイルのアップロード
* ディレクトリのアップロード
### 説明
それぞれBitcasaのキャッシュを意識してコピーするようにしてある。
### Example
#### 単体ファイルのアップロード
`upload.sh -f file.txt upload_path`
#### ディレクトリのアップロード
`upload.sh -d directory upload_path`

## chk_exist.sh
### 機能
* アップロード先に指定したファイルがあるか確認し"exist"か"nothing"を表示する
* アップロード先に指定したディレクトリがあるか確認し"exist"か"nothing"を表示する
### 説明
ファイルやディレクトリの存在確認のためのスクリプト
### Howto
#### ファイルもしくはディレクトリの存在確認
`chk_exist.sh [file | dir] [file path]`
